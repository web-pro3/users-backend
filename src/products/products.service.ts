/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

// eslint-disable-next-line prefer-const
let products: Product[] = [
  { id: 1, name: 'kanom', price: 100 },
  { id: 2, name: 'loog ohm', price: 200 },
  { id: 3, name: 'namkin', price: 300 },
];
// eslint-disable-next-line prefer-const
let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProducts: Product = {
      id: lastProductId++,
      ...createProductDto, //login, name, password
    };

    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('upsate' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'kanom', price: 100 },
      { id: 2, name: 'loog ohm', price: 200 },
      { id: 3, name: 'namkin', price: 300 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
